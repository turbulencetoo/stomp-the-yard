
TMPDOT = graphboy.dot

all: out.svg

out.svg: graphboy.py
	python3 graphboy.py > ${TMPDOT}
	< ${TMPDOT} dot -Tsvg -o $(@)

out.pdf: graphboy.py
	python3 graphboy.py > ${TMPDOT}
	< ${TMPDOT} dot -Tpdf -o $(@)

neato_out.svg:	graphboy.py
	python3 graphboy.py > ${TMPDOT}
	< ${TMPDOT} neato -Tsvg -o $(@)


clean:
	rm ${TMPDOT} out.svg
