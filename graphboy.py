from graphviz import Graph

RED = "#ff0000"
BLUE = "#0000ff"
GREEN = "#20ff20"
LIGHTGREEN = "#C2FCC2"
BLACK = "#000000"
ORANGE = "#FFA024"

TAXI = "T"
BUS = "B"
UNDERGROUND = "U"
FERRY = "F"

FNAME = "SCOTMAP.TXT"
ELLISFNAME = "ELLISISLAND.TXT"
RIVERNAME = "SCOTRIVER.TXT"

FONTNAME = "Helvetica-Bold"


class Node:
    def __init__(self, _id):
        self.idx = _id

        self.trans_dict = {TAXI: True,
                           BUS: False,
                           UNDERGROUND: False,
                           FERRY: False
                           }

        self.edges = set()

    def add_edge(self, edge):
        self.edges.add(edge)
        self.trans_dict[edge.trans_type] = True

class Edge:

    def __init__(self, id1, id2, trans_type):
        self.id1 = id1
        self.id2 = id2
        self.trans_type = trans_type

    def __hash__(self):
        return hash((self.id1, self.id2, self.trans_type))


def parsefile(file, id_to_node_dict, edge_set):

    lines = []  # List of tuples (id1, id2, trans_type)
    with open(FNAME) as fp:
        for line in fp:
            line = line.strip()
            line = line.split()
            lines.append(line)

    for line in lines:
        id1, id2, trans_type = line
        new_edge = Edge(id1, id2, trans_type)
        edge_set.add(new_edge)
        for idx in (id1,id2):
            if idx not in id_to_node_dict:
                id_to_node_dict[idx] = Node(idx)
            id_to_node_dict[idx].add_edge(new_edge)


def populate_ellis_island(ellis_island_set):

    with open(ELLISFNAME) as fp:
        for line in fp:
            line = line.strip()
            ellis_island_set.add(line)


def make_river(main_graph):

    river = []
    with open(RIVERNAME) as f:
        for line in f:
            line = line.strip()
            line = line.split()
            river.append( (line[0], line[1]) )
    id_num = 100
    id_prefix= "f"
    prev_id = None
    for nodes in river:
        node_id = str(id_num) + id_prefix

        main_graph.node(node_id, fontsize='1', width="0.01", style="invis")
        main_graph.edge(nodes[0], node_id, style="invis")
        main_graph.edge(node_id, nodes[1], style="invis")

        if prev_id:
            main_graph.edge(node_id, prev_id, color=BLUE, penwidth="15")

        prev_id = node_id
        id_num += 1


def make_dot(id_to_node_dict, edge_set):

    main_graph = Graph(comment="Stomp the Yard")
    main_graph.attr('graph', ratio='0.7727')  # 8.5"/11"
    main_graph.attr('graph', clusterMode='local')


    main_graph.attr('graph', overlap='false')  # needed for 'neato' engine 
    main_graph.attr('graph', splines='true')  # needed for 'neato' engine 

    for node in id_to_node_dict.values():
        make_node(main_graph, node)

    #make_river(main_graph)

    for edge in edge_set:
        edgecolor = {TAXI: BLACK,
                     BUS: GREEN,
                     UNDERGROUND: ORANGE,
                     FERRY: BLUE}[edge.trans_type]
        edgewidth = {TAXI: "2",
                     BUS: "2",
                     UNDERGROUND: "3",
                     FERRY: "2"}[edge.trans_type]
        main_graph.edge(edge.id1, edge.id2, color=edgecolor, penwidth=edgewidth)

    return main_graph


def make_node(dot, node):

    #options for all nodes:
    font_color = BLACK
    font_size = "22"
    font_name = FONTNAME
    pen_width = "2"
    node_color = BLACK
    node_shape = "circle"
    node_width = "0.7"
    node_style = "solid"
    node_fill = ""

    
    if node.trans_dict[BUS]:
        node_shape = "Mcircle"
        node_style = 'filled'
        node_fill = LIGHTGREEN
    # Order of these 'if' blocks matters because both set node_shape 
    if node.trans_dict[UNDERGROUND]:
        node_shape = "doublecircle"
        node_color = ORANGE

    dot.node(node.idx, color=node_color, fontcolor=font_color, shape=node_shape, fontname=font_name,
             fontsize=font_size, penwidth=pen_width, width=node_width, style=node_style, fillcolor=node_fill)


def main():
    id_to_node_dict = {}
    edge_set = set()

    parsefile(FNAME, id_to_node_dict, edge_set)

    dot = make_dot(id_to_node_dict, edge_set)


    print(dot.source)

if __name__ == "__main__":
    main()